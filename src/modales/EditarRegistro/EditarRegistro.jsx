import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Formik, Form as Formulario , Field } from 'formik';
import toast, { Toaster } from 'react-hot-toast';
import { Baseurl } from '../../store/Baseurl';

//boostrap
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';

//material iu
import Buttonui from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import CloseIcon from '@mui/icons-material/Close';
import LoadingButton from '@mui/lab/LoadingButton';


//css
import './style/index.css';
import { useDispatch, useSelector } from 'react-redux';
import { datosmodal } from '../../store/home/homeSlice';

export const EditarRegistro = (props) => {
    
    const [loading, setLoading] = useState(false);
    // const [datos,setdatos] = useState({});
    const id = props.id;
    const cargar = props.id;

    const dispatch = useDispatch();
    const datos = useSelector(state => state.home.datosmodal);

    useEffect(() => {
        const getDatos = async() =>{
      
            const token = document.cookie.replace('token=',''); 
            const { data } = await axios({
              method:'GET',
              url:`${Baseurl}embarque/${id}`,
              // headers:{ Authorization: `Bearer ${token}`,
              //           Accept :'application/json', 
              // }
            });
                // setdatos(data);
                dispatch(datosmodal(data));
                console.log(data);
          }
        getDatos();

    }, [cargar])

    console.log(datos.cliente);

    return (
        <>
        <Modal {...props} size="xl" aria-labelledby="contained-modal-title-vcenter" className='modal'>
      <Modal.Header className='header-modal'>
        
        <CloseIcon type="button" className='equis' onClick={props.onHide}/>
        
        <Modal.Title id="contained-modal-title-vcenter" className='title-modal'>
          Editar Embarque con el id: {props.id}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid body-modal">
        <Container>
          
        <Toaster/>
    <Formik  
        initialValues={{
          fecha: datos.fecha,
          cliente: datos.cliente,
          factura: datos.factura,
          embarque: datos.embarque,
          ruta: datos.ruta,
          tipo: datos.tipo,
          tracto: datos.tracto,
          chofer: datos.chofer,
          cajas: datos.cajas,
          efectivo: datos.efectivo,
          observaciones: datos.observaciones

        }}
        onSubmit={(valores, { resetForm }) =>{
            setLoading(true);
          
            console.log(valores);
            const enviar = async() =>{
              
              const token = document.cookie.replace('token=',''); 

                const { data } = await axios({
                    method:'put',
                    url:`${Baseurl}new_embarque/${id}`,
                    data:valores,
                    // headers:{ Authorization: `Bearer ${token}`,
                    //           Accept :'application/json', 
                    // }
                });
                    console.log(data);

                    setLoading(false);

            if(data.respuesta){
                toast.success(data.respuesta);
                // dispatch(getdatos());

            }
        }
          enviar();

        }}
    >
      {() =>(
      <Formulario>
        <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Fecha:</Form.Label>
              <Field
                className='input-form'
                id='fecha'
                name='fecha'
                type="text"
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cliente:</Form.Label>
              <Field
                className='input-form'
                id='cliente'
                name='cliente'
                type="text"
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Factura:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='factura'
                name='factura'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Embarque:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='embarque'
                name='embarque'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Ruta:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='ruta'
                name='ruta'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tipo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tipo'
                name='tipo'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tracto:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tracto'
                name='tracto'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Chofer:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='chofer'
                name='chofer'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cajas:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='cajas'
                name='cajas'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Efectivo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='efectivo'
                name='efectivo'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={12} lg={12}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlTextarea1">
              <Form.Label className='label-form'>Observaciones:</Form.Label>
              <Field 
                className='input-form'
                as="textarea" 
                rows={4} 
                id='observaciones'
                name='observaciones'
                
              />
            </Form.Group>
            </Col>
            
          </Row>

          <LoadingButton
                type='submit'
                loading={loading}
                variant="outlined"
                ><span>Guardar Cambios</span>
            </LoadingButton>

        </Formulario>
    )
    
    }
    </Formik>
        

        </Container>
      </Modal.Body>
      <Modal.Footer className='footer-modal'>
        {/* <Button className='btn-info'>Guardar</Button> */}
        <Button className='btn-danger' onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
        </>
    );
};


