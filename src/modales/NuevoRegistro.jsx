import React, { useEffect, useState } from 'react';

//boostrap
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Modal from 'react-bootstrap/Modal';

//material iu
import Buttonui from '@mui/material/Button';
import Stack from '@mui/material/Stack';
import CloseIcon from '@mui/icons-material/Close';

import './NuevoRegistro.css';

//modales
import { Generales } from '../modalesComponents/Generales';
import { Gastos } from '../modalesComponents/Gastos';
import { Observaciones } from '../modalesComponents/Observaciones';

export const NuevoRegistro = (props) => {
const [btngenerales, setbtngenerales] = useState(false);
const [btngastos, setbtngastos] = useState(false);
const [btnobservaciones, setbtnobservaciones] = useState(false);

useEffect(() => {
 setbtngenerales(true);
}, [])


const generales = () =>{
  setbtnobservaciones(false);
  setbtngastos(false);
  setbtngenerales(true);
}
const gastos = () =>{
  setbtngenerales(false);
  setbtnobservaciones(false);
  setbtngastos(true);
}
const observaciones = () =>{
  setbtngenerales(false);
  setbtngastos(false);
  setbtnobservaciones(true);
}

  return (
    <Modal {...props} size="xl" aria-labelledby="contained-modal-title-vcenter" className='modal'>
      <Modal.Header className='header-modal'>
        
        <CloseIcon type="button" className='equis' onClick={props.onHide}/>
        
        <Modal.Title id="contained-modal-title-vcenter" className='title-modal'>
          Nuevo Embarque
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid body-modal">
        <Container>
          
        <Stack 
          direction="row" 
          justifyContent="flex-end"
          spacing={2}
          alignItems="center"
          className='mb-4' 
        >
          { (btngenerales === true)
            ?<><Buttonui id='btn-iu' variant="outlined" onClick={generales} >Generales</Buttonui></>  
            :<><Buttonui id='btn2-iu' variant="outlined" onClick={generales} >Generales</Buttonui></> 
          }
          { (btngastos === true)
            ?<><Buttonui id='btn-iu' variant="outlined" onClick={gastos}>Gastos</Buttonui></>  
            :<><Buttonui id='btn2-iu' variant="outlined" onClick={gastos}>Gastos</Buttonui></> 
          }
          { (btnobservaciones === true)
            ?<><Buttonui id='btn-iu' variant="outlined" onClick={observaciones}>Observaciones</Buttonui></>  
            :<><Buttonui id='btn2-iu' variant="outlined" onClick={observaciones}>Observaciones</Buttonui></> 
          }
          
          
            
        </Stack>

        { (btngenerales === true)?<><Generales/></>  :<></> }
        { (btngastos === true)?<><Gastos/></>  :<></> }
        { (btnobservaciones === true)?<><Observaciones/></>  :<></> }
        

        </Container>
      </Modal.Body>
      <Modal.Footer className='footer-modal'>
        {/* <Button className='btn-info'>Guardar</Button> */}
        <Button className='btn-danger' onClick={props.onHide}>Close</Button>
      </Modal.Footer>
    </Modal>
  )
}
