import { useEffect, useState, useRef } from 'react';
import axios from 'axios';
import { downloadExcel } from 'react-export-table-to-excel';
import Swal from 'sweetalert2';
import { Formik, Form as Formulario , Field } from 'formik';
import toast, { Toaster } from 'react-hot-toast';
import _ from "lodash";
import DatePicker from 'react-datepicker';
import 'react-datepicker/dist/react-datepicker.css';

//store global
import { Baseurl } from '../../store/Baseurl';
import { Loading } from '../../store/loading';
import { useDispatch, useSelector } from 'react-redux';
// import { homeSlice } from '../../store/home/homeSlice';
import { actualdatos, getdatos } from '../../store/home/homeSlice';

//componentes pagination
import TablaReporte from './components/TablaReporte/';

//componentes golbales
import { Footer } from '../../components/Footer'
import { Header } from '../../components/Header'
import { Menu } from '../../components/Menu'

//modales
import { NuevoRegistro } from '../../modales/NuevoRegistro';
import { VerRegistro } from '../../modales/VerRegistro/VerRegistro';
import { EditarRegistro } from '../../modales/EditarRegistro/EditarRegistro';

//bootstrap
import Table from 'react-bootstrap/Table';
import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';

//material ui
import LoadingButton from '@mui/lab/LoadingButton';
import DownloadIcon from '@mui/icons-material/Download';
import Fab from '@mui/material/Fab';
import PictureAsPdfIcon from '@mui/icons-material/PictureAsPdf';
import DescriptionIcon from '@mui/icons-material/Description';
import AddIcon from '@mui/icons-material/Add';
import Stack from '@mui/material/Stack';
import Buttoniu from '@mui/material/Button';
import CloseIcon from '@mui/icons-material/Close';

//css
import './style/index.css';

const Home = () => {

  const [loading, setloading]                 = useState(false);
  const [downloading, setdownloading]         = useState(false);
  const [registroloading, setregistroloading] = useState(false);

  const [modalShow, setModalShow] = useState(false);

  const [modalVerRegistro, setModalVerRegistro] = useState(false);
  const [modalEditarRegistro, setModalEditarRegistro] = useState(false);
  
  //modal reportes
  const [modalReportes, setModalReportes] = useState(false);
  const [desde,setdesde] = useState(null);
  const [hasta,sethasta] = useState(null);
  const [desdetimestamp,setdesdetimestamp] = useState(null);
  const [hastatimestamp,sethastatimestamp] = useState(null);
  const [exceldatos, setexceldatos] = useState([]);
  
  const [loadingformulario, setloadingformulario] = useState(false);

  const [id,setid] = useState(null);

  const[modaldatos,setmodaldatos] = useState({});
  const [modalLoading, setmodalLoading] = useState(false);
  
  const [paginationTable, setpaginationTable] = useState();
  const pageSize = 5;

  //store
  const dispatch = useDispatch();
  const datos = useSelector(state => state.home.datos);
  const actualizardatos = useSelector(state => state.home.actualizardatos);
  
  useEffect(() => {
    
    const getDatos = async() =>{
      
      const token = document.cookie.replace('token=',''); 
      const { data } = await axios({
        method:'GET',
        url:`${Baseurl}new_embarque`,
        // headers:{ Authorization: `Bearer ${token}`,
        //           Accept :'application/json', 
        // }
      });
      dispatch(actualdatos(data));
      setpaginationTable(_(data).slice(0).take(pageSize).value());
    }
  getDatos();
  
}, [actualizardatos])

const fechaActual = () =>{  
const fecha = new Date();
const hoy = fecha.toLocaleDateString();
const formato = hoy.replaceAll('/', '.');

return formato;
}

  const verpdf = async() =>{
    setloading(true);
    console.log(desdetimestamp,hastatimestamp);
    const resp = await axios({
        method:'POST',
        url:`${Baseurl}verpdf/`,
        data:{desdetimestamp,hastatimestamp},
        responseType:"blob",
        }
    );
    const url = window.URL.createObjectURL(new Blob([resp.data],{type: 'application/pdf'}));
    setloading(false);
    window.open(url, "_blank");
  }

  const excel = async() =>{
    setdownloading(true);
    
    const header = [
      'id',
      'fecha',
      'factura',
      'embarque',
      'cliente',
      'tipo',
      'tracto',
      'cajas',
      'ruta',
    ];

    const { data } = await axios({
      method:'POST',
      url:`${Baseurl}descargarpdf/`,
      data:{desdetimestamp,hastatimestamp},
      }
  );

  downloadExcel({
    fileName: `reporte-${fechaActual()}.xls`,
    sheet: 'reporte',
    tablePayload: { 
      header:header ,  
      body:data, 
    } ,
  });
  
  setdownloading(false);

  }
  // const descargarpdf = async() =>{
  //   setdownloading(true);
  //   const resp = await axios.get(
  //     `${Baseurl}verpdf`,{responseType:"blob"}
  //   );
  //   const url = window.URL.createObjectURL(new Blob([resp.data]));
  //   const link=document.createElement('a');
  //   link.href = url;
  //   link.download = `Reporte-${formato}.pdf`;
  //   // console.log(link.download);
  //   link.click();
  //   setdownloading(false);
   
  // }

  const nuevoRegistro= () => {
    setregistroloading(true);
    setModalShow(true);
    setregistroloading(false);
  }

  const ver = (id) =>{
    
    const getDatos = async(id) =>{
      
      const token = document.cookie.replace('token=',''); 
      const { data } = await axios({
        method:'GET',
        url:`${Baseurl}embarque/${id}`,
        // headers:{ Authorization: `Bearer ${token}`,
        //           Accept :'application/json', 
        // }
      });
          setmodaldatos(data);
          setModalVerRegistro(true);
    }
  getDatos(id);

  }

  const editar = (id) =>{

    const getDatos = async(id) =>{
      
      const token = document.cookie.replace('token=',''); 
      const { data } = await axios({
        method:'GET',
        url:`${Baseurl}embarque/${id}`,
        // headers:{ Authorization: `Bearer ${token}`,
        //           Accept :'application/json', 
        // }
      });
          setmodaldatos(data);
          // dispatch(datosmodal(data));
          setModalEditarRegistro(true);
    }
  getDatos(id);
    
  }
 
  const eliminar = async(id) =>{
    Swal.fire({
      icon: 'question',
      title:'Eliminar',
      text: `¿Seguro que quieres eliminar el registro con el id ${id}?`,
      showDenyButton: true,
      cancelButtonText: 'Cancelar',
      showConfirmButton: true,
      confirmButtonText: 'Confirmar',
    }).then(response=>{
      if(response.isConfirmed){
      
        const eliminarRegistro = async(id) =>{
              
            const token = document.cookie.replace('token=',''); 
            const { data } = await axios({
                method:'delete',
                url:`${Baseurl}embarque/${id}`,
                // headers:{ Authorization: `Bearer ${token}`,
                //           Accept :'application/json', 
                // }
            });
                dispatch(getdatos());
        
          Swal.fire({
              icon:'success',
              title:'Exito',
              text:'el registro se eliminó correctamente.',
              timer: 4000,
          });
        
    }
    eliminarRegistro(id);

          
      }
      
    });
  }

  const onchangeDesde =(fecha)=>{
    setdesde(fecha);
  }
  const onchangeHasta =(fecha)=>{
    sethasta(fecha);
  }

  const formulario = () =>{
    setloadingformulario(false);
    setdesde(null);
    sethasta(null);
  }
  return (
    <>
    
   <div className="wrapper">
    <Header/>
    <Menu/>  

  {/* Content Wrapper. Contains page content */}
  <div className="content-wrapper">
    {/* Content Header (Page header) */}
    <section className="content-header ">
      <div className="container-fluid ">
        <div className="row mb-2">
          <div className="col-sm-6">
            <h1>Registro de Embarques</h1>
          </div>

          {/* <div className="col-sm-6">
            <ol className="breadcrumb float-sm-right">
              <li className="breadcrumb-item"><a href="#">Home</a></li>
              <li className="breadcrumb-item active">Reporte 1</li>
            </ol>
          </div> */}

        </div>
      </div>{/* /.container-fluid */}
    </section>
    {/* Main content */}
    <section className="content">
      {/* Default box */}
      <div className="card">
        <div className="card-header">
          {/* <h3 className="card-title"></h3> */}
        
          
        <LoadingButton
          onClick={() => setModalReportes(true)}
          startIcon={<DescriptionIcon />}
          loading={loading}
          loadingPosition="start"
          variant="contained"
          className='ml-5 mr-2'
          id='boton-ver'
        >
          Reportes
        </LoadingButton>
          
        <Buttoniu 
            variant="outlined"
            onClick={nuevoRegistro}
            id='btn-registro'
            ><AddIcon />
        </Buttoniu>

          {/* modales externos */}
          <NuevoRegistro  
              show={modalShow} 
              onHide={() => setModalShow(false)} 
          />
          {/* modales externos*/}

          <div className="card-tools">
           
           
          </div>
        </div>
        <div className="card-body">
          
      <TablaReporte 
        datos={datos}
        paginationTable={paginationTable}
        setpaginationTable={setpaginationTable}
        pageSize={pageSize}
        />

        </div>
        {/* /.card-body */}
        <div className="card-footer text-footer">
          Mostrando 1 a 3 de 10
        </div>
        {/* /.card-footer*/}
      </div>
      {/* /.card */}
    </section>
    {/* /.content */}
  </div>
  
    <Footer/>
  
</div>

{/* modal editar */}
<Modal show={modalEditarRegistro} size="xl" aria-labelledby="contained-modal-title-vcenter" className='modal'>
      <Modal.Header className='header-modal'>
        
        <CloseIcon type="button" className='equis' onClick={() => setModalEditarRegistro(false)}/>
        
        <Modal.Title id="contained-modal-title-vcenter" className='title-modal'>
          Editar Embarque con el id: {modaldatos.id}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid body-modal">
        <Container>
          
        <Toaster/>
    <Formik  
        initialValues={{
          fecha: modaldatos.fecha,
          cliente: modaldatos.cliente,
          factura: modaldatos.factura,
          embarque: modaldatos.embarque,
          ruta: modaldatos.ruta,
          tipo: modaldatos.tipo,
          tracto: modaldatos.tracto,
          chofer: modaldatos.chofer,
          cajas: modaldatos.cajas,
          efectivo: modaldatos.efectivo,
          observaciones: modaldatos.observaciones

        }}
        onSubmit={(valores, { resetForm }) =>{
          setmodalLoading(true);
          
            const enviar = async() =>{
              
              const token = document.cookie.replace('token=',''); 

                const { data } = await axios({
                    method:'put',
                    url:`${Baseurl}embarque/${modaldatos.id}`,
                    data:valores,
                    // headers:{ Authorization: `Bearer ${token}`,
                    //           Accept :'application/json', 
                    // }
                });
                    dispatch(getdatos());
                    setmodalLoading(false);

            if(data.respuesta){
                toast.success(data.respuesta);
                // dispatch(getdatos());

            }
        }
          enviar();

        }}
    >
      {() =>(
      <Formulario>
        <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Fecha:</Form.Label>
              <Field
                className='input-form'
                id='fecha'
                name='fecha'
                type="text"
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cliente:</Form.Label>
              <Field
                className='input-form'
                id='cliente'
                name='cliente'
                type="text"
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Factura:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='factura'
                name='factura'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Embarque:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='embarque'
                name='embarque'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Ruta:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='ruta'
                name='ruta'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tipo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tipo'
                name='tipo'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tracto:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tracto'
                name='tracto'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Chofer:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='chofer'
                name='chofer'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cajas:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='cajas'
                name='cajas'
                
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Efectivo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='efectivo'
                name='efectivo'
                
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={12} lg={12}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlTextarea1">
              <Form.Label className='label-form'>Observaciones:</Form.Label>
              <Field 
                className='input-form'
                as="textarea" 
                rows={4} 
                id='observaciones'
                name='observaciones'
                
              />
            </Form.Group>
            </Col>
            
          </Row>

          <LoadingButton
                type='submit'
                loading={modalLoading}
                variant="outlined"
                ><span>Guardar Cambios</span>
            </LoadingButton>

        </Formulario>
    )
    
    }
    </Formik>
        

        </Container>
      </Modal.Body>
      <Modal.Footer className='footer-modal'>
        {/* <Button className='btn-info'>Guardar</Button> */}
        <Button className='btn-danger' onClick={() => setModalEditarRegistro(false)}>Close</Button>
      </Modal.Footer>
    </Modal>
{/* modal editar */}

{/* modal ver */}
<Modal show={modalVerRegistro} size="xl" aria-labelledby="contained-modal-title-vcenter" className='modal'>
      <Modal.Header className='header-modal'>
        
        <CloseIcon type="button" className='equis' onClick={() => setModalVerRegistro(false)}/>
        
        <Modal.Title id="contained-modal-title-vcenter" className='title-modal'>
          VER Embarque con el id: {modaldatos.id}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid body-modal">
        <Container>
          
        <Toaster/>
    <Formik  
        initialValues={{
          fecha: modaldatos.fecha,
          cliente: modaldatos.cliente,
          factura: modaldatos.factura,
          embarque: modaldatos.embarque,
          ruta: modaldatos.ruta,
          tipo: modaldatos.tipo,
          tracto: modaldatos.tracto,
          chofer: modaldatos.chofer,
          cajas: modaldatos.cajas,
          efectivo: modaldatos.efectivo,
          observaciones: modaldatos.observaciones

        }}
        
    >
      {() =>(
      <Formulario>
        <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Fecha:</Form.Label>
              <Field
                className='input-form'
                id='fecha'
                name='fecha'
                type="text"
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cliente:</Form.Label>
              <Field
                className='input-form'
                id='cliente'
                name='cliente'
                type="text"
                readOnly
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Factura:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='factura'
                name='factura'
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Embarque:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='embarque'
                name='embarque'
                readOnly
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Ruta:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='ruta'
                name='ruta'
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tipo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tipo'
                name='tipo'
                readOnly
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tracto:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tracto'
                name='tracto'
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Chofer:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='chofer'
                name='chofer'
                readOnly
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cajas:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='cajas'
                name='cajas'
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Efectivo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='efectivo'
                name='efectivo'
                readOnly
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={12} lg={12}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlTextarea1">
              <Form.Label className='label-form'>Observaciones:</Form.Label>
              <Field 
                className='input-form'
                as="textarea" 
                rows={4} 
                id='observaciones'
                name='observaciones'
                readOnly
              />
            </Form.Group>
            </Col>
            
          </Row>

          {/* <LoadingButton
                type='submit'
                loading={loading}
                variant="outlined"
                ><span>Guardar</span>
            </LoadingButton> */}
        </Formulario>
    )
    
    }
    </Formik>
        

        </Container>
      </Modal.Body>
      <Modal.Footer className='footer-modal'>
        {/* <Button className='btn-info'>Guardar</Button> */}
        <Button className='btn-danger' onClick={() => setModalVerRegistro(false)}>Close</Button>
      </Modal.Footer>
    </Modal>
{/* modal ver */}

{/* modal Reportes */}
<Modal show={modalReportes} size="xl" aria-labelledby="contained-modal-title-vcenter" className='modal'>
      <Modal.Header className='header-modal'>
        
        <CloseIcon type="button" className='equis' onClick={() => setModalReportes(false)}/>
        
        <Modal.Title id="contained-modal-title-vcenter" className='title-modal'>
          Reportes
        </Modal.Title>
      </Modal.Header>
      <Modal.Body className="show-grid body-modal">
        <Container>
          
        <Toaster/>
    <Formik  
        initialValues={{
          // desde: '',
          // hasta: ''

        }}
        onSubmit={(valores, { resetForm }) =>{
            
            
            let desdetime = Math.round(desde.getTime() / 1000);
            let hastatime = Math.round(hasta.getTime() / 1000);
            resetForm();
            
            if(desdetime === hastatime){
              desdetime = (desdetime - 86400);
            }
            hastatime = (hastatime + 86400);
            setdesdetimestamp(desdetime);
            sethastatimestamp(hastatime);
            setloadingformulario(true);
        }}
    >
      {() =>(
      <Formulario>
        <Row>
            <Col xs={4} md={4} lg={4}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Desde:</Form.Label>
              <DatePicker
                className='input-form'
                id='desde'
                name='desde'
                selected={desde}
                onChange={onchangeDesde}
                
              />
            </Form.Group>
            </Col>
            <Col xs={4} md={4} lg={4}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Hasta:</Form.Label>
              <DatePicker
                className='input-form'
                id='hasta'
                name='hasta'
                selected={hasta}
                onChange={onchangeHasta}
                
              />
            </Form.Group>
            </Col>
            <Col xs={4} md={4} lg={4}>
            {
              (desde === null || hasta === null)
              ?<LoadingButton
                    type='submit'
                    loading={loading}
                    variant="outlined"
                    disabled
                    ><span>Buscar</span>
                </LoadingButton>
              :<LoadingButton
                    type='submit'
                    loading={loading}
                    variant="outlined"
                    ><span>Buscar</span>
                </LoadingButton>
            }  
              
            </Col>
          </Row>
          

          
        </Formulario>
    )
    
    }
    </Formik>
       {
        (loadingformulario === true)
        ?<>
        <LoadingButton
          onClick={excel}
          startIcon={<DescriptionIcon />}
          loading={downloading}
          loadingPosition="start"
          variant="contained"
          className='ml-3 mr-2'
          id='boton-download'
        >
          excel
        </LoadingButton>

          <LoadingButton
          onClick={verpdf}
          startIcon={<PictureAsPdfIcon />}
          loading={loading}
          loadingPosition="start"
          variant="contained"
          className='ml-5 mr-2'
          id='boton-ver'
        >
          PDF
        </LoadingButton>

        <LoadingButton
          onClick={formulario}
          loadingPosition="start"
          variant="contained"
          className='ml-5 mr-2'
          id='boton-ver'
        >
          Eliminar fechas
        </LoadingButton>
        </>
        :<></>
       }
    
       

        </Container>
      </Modal.Body>
      <Modal.Footer className='footer-modal'>
        {/* <Button className='btn-info'>Guardar</Button> */}
        <Button className='btn-danger' onClick={() => setModalReportes(false)}>Close</Button>
      </Modal.Footer>
    </Modal>
{/* modal Reportes */}
    </>
  )
}

export default Home