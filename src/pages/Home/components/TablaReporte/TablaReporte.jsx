
import { useState } from 'react';
import {v4 as uuid } from 'uuid';


import DeleteIcon from '@mui/icons-material/Delete';
import VisibilityIcon from '@mui/icons-material/Visibility';
import EditIcon from '@mui/icons-material/Edit';

//bootstrap
import Table from 'react-bootstrap/Table';

import _ from "lodash";
import './style/index.css';

const TablaReporte = ({datos, paginationTable,setpaginationTable,pageSize}) => {

  
  const pageCount = datos? Math.ceil(datos.length/pageSize) : 0;
  if(pageCount === 1) return null;
  const pages = _.range(1, pageCount+1);

  const [currentPage, setcurrentPage] = useState();

  const pagination = (pageNo)=>{
    setcurrentPage(pageNo);
    const startIndex = (pageNo -1) * pageSize;
    const paginatedPost = _(datos).slice(startIndex).take(pageSize).value();
    setpaginationTable(paginatedPost);
  }

  console.log("soy la tabla");

  return (
        <>
        {!paginationTable ? ("No hay datos"):(

          <Table striped bordered hover >
      <thead>
        <tr className='tr-table'>
          <th>ID</th>
          <th>FECHA</th>
          <th>FACTURA</th>
          <th>EMBARQUE</th>
          <th>CLIENTE</th>
          <th>TIPO</th>
          <th>TRACTO</th>
          <th>CAJA</th>
          <th>LOGISTICA</th>
          <th>Acciones</th>
        </tr>
      </thead>
      <tbody>
        {/* tr1-table, tr2-table */}

       
      {
        paginationTable.map((datos , i) => {
          const num = (i+1);
          return(
            <>
              {(num%2==0)
                ?<tr id='tr2-table' key={uuid()}>
                    <td>{datos.id}</td>
                    <td>{datos.fecha}</td>
                    <td>{datos.factura}</td>
                    <td>{datos.embarque}</td>
                    <td>{datos.cliente}</td>
                    <td>{datos.tipo}</td>
                    <td>{datos.tracto}</td>
                    <td>{datos.cajas}</td>
                    <td>{datos.ruta}</td>
                    <td className='icons'>
                      <VisibilityIcon 
                            className='icon'
                            onClick={() => ver(datos.id)}
                            />
                      <EditIcon 
                        className='icon'
                        onClick={() => editar(datos.id)} 
                        />
                      <DeleteIcon 
                          className='icon' 
                          onClick={() => eliminar(datos.id)}
                          />
                    </td>
                  </tr>
                :<tr id='tr1-table' key={`${uuid()}`}>
                    <td>{datos.id}</td>
                    <td>{datos.fecha}</td>
                    <td>{datos.factura}</td>
                    <td>{datos.embarque}</td>
                    <td>{datos.cliente}</td>
                    <td>{datos.tipo}</td>
                    <td>{datos.tracto}</td>
                    <td>{datos.cajas}</td>
                    <td>{datos.ruta}</td>
                    <td className='icons'>
                      <VisibilityIcon 
                          className='icon' 
                          onClick={() => ver(datos.id)}
                          />
                      <EditIcon 
                        className='icon'
                        onClick={() => editar(datos.id)} 
                        />
                      <DeleteIcon 
                          className='icon' 
                          onClick={() => eliminar(datos.id)}
                          />
                    </td>
                  </tr>
              }
             
            </>
          )
        })
      }
        
      </tbody>
    </Table>
      )}
      
      <nav className='d-flex justify-content-end mt-4'>
        <ul className='pagination'>
          {
            pages.map((page)=>(
              <li className={
                page === currentPage? "page-item active" : "page-item" 
              }>
               <p className='page-link'
                  onClick={() => pagination(page)}
                  >{page}</p> 
              </li>
            ))
          }
        </ul>
      </nav>
    </>
    );
};

export default TablaReporte
