import { useState } from 'react';
import axios from 'axios';
import { Formik, Form as Formulario , Field } from 'formik';
import { Baseurl } from '../store/Baseurl';
import toast, { Toaster } from 'react-hot-toast';

//bootstrap
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';

//material ui
import LoadingButton from '@mui/lab/LoadingButton';

//store
import { useDispatch } from 'react-redux';
import { getdatos } from '../store/home/homeSlice';
import Home from '../pages/Home/Home';

export const Generales = () => {

  const [loading, setLoading] = useState(false);
  const fecha = new Date(); 
  const hoy = fecha.toLocaleDateString();

  //store
  const dispatch = useDispatch();

  return (
    <>
    <Toaster/>
    <Formik  
        initialValues={{
          fecha:hoy,
          cliente: '',
          factura:'',
          embarque:'',
          ruta:'',
          tipo:'',
          tracto:'',
          chofer:'',
          cajas:'',
          efectivo:'',
          observaciones:''

        }}
        onSubmit={(valores, { resetForm }) =>{
            setLoading(true);
          
            console.log(valores);
            const enviar = async() =>{
              
              const token = document.cookie.replace('token=',''); 

                const { data } = await axios({
                    method:'POST',
                    url:`${Baseurl}new_embarque`,
                    data:valores,
                    // headers:{ Authorization: `Bearer ${token}`,
                    //           Accept :'application/json', 
                    // }
                });
                    console.log(data);

                    setLoading(false);

            if(data.respuesta){
                toast.success(data.respuesta);
                resetForm();
                dispatch(getdatos());

            }
        }
          enviar();

        }}
    >
      {() =>(
      <Formulario>
        <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Fecha:</Form.Label>
              <Field
                className='input-form'
                id='fecha'
                name='fecha'
                type="text"
                readOnly
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cliente:</Form.Label>
              <Field
                className='input-form'
                id='cliente'
                name='cliente'
                type="text"
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Factura:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='factura'
                name='factura'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Embarque:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='embarque'
                name='embarque'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Ruta:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='ruta'
                name='ruta'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tipo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tipo'
                name='tipo'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Tracto:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='tracto'
                name='tracto'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Chofer:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='chofer'
                name='chofer'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={8} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Cajas:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='cajas'
                name='cajas'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
            <Col xs={6} md={4} lg={6}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlInput1">
              <Form.Label className='label-form'>Efectivo:</Form.Label>
              <Field
                className='input-form'
                type="text"
                id='efectivo'
                name='efectivo'
                placeholder=""
                autoFocus
              />
            </Form.Group>
            </Col>
          </Row>
          <Row>
            <Col xs={12} md={12} lg={12}>
            <Form.Group className="mb-3 flex-row" controlId="exampleForm.ControlTextarea1">
              <Form.Label className='label-form'>Observaciones:</Form.Label>
              <Field 
                className='input-form'
                as="textarea" 
                rows={4} 
                id='observaciones'
                name='observaciones'
              />
            </Form.Group>
            </Col>
            
          </Row>

          <LoadingButton
                type='submit'
                loading={loading}
                variant="outlined"
                ><span>Guardar</span>
            </LoadingButton>
        </Formulario>
    )
    
    }
    </Formik>
  </>
  )
}