import { useState } from 'react';
import axios from 'axios';
import { Formik, Form as Formulario , Field } from 'formik';
import { Baseurl } from '../store/Baseurl';
import toast, { Toaster } from 'react-hot-toast';

//boostrap
import Row from 'react-bootstrap/Row';
import Form from 'react-bootstrap/Form';
import Col from 'react-bootstrap/Col';

//material UI
import Buttoniu from '@mui/material/Button';
import AddIcon from '@mui/icons-material/Add';
import Stack from '@mui/material/Stack';
import LoadingButton from '@mui/lab/LoadingButton';

//css
import './Gastos.css';

export const Gastos = () => {

  const [loading, setLoading] = useState(false);


  return (
    <>
    <Toaster/>
    <Formik
    initialValues={{
      Tiket: '',
      concepto: '',
      cantidad: '',
      precioUnitario: '',
      importe: '',
      observaciones:'',
    }}
    onSubmit={(valores, { resetForm }) =>{
        setLoading(true);
      
        console.log(valores);
        const enviar = async() =>{
          
          const token = document.cookie.replace('token=',''); 

            const { data } = await axios({
                method:'POST',
                url:`${Baseurl}nuevoticket/`,
                data:valores,
                // headers:{ Authorization: `Bearer ${token}`,
                //           Accept :'application/json', 
                // }
            });
                console.log(data);

                setLoading(false);

        if(data.respuesta){
            toast.success(data.respuesta);
            resetForm();
            // dispatch(getdatos());

        }
    }
      enviar();

    }}
    >
      {()=>(
        <Formulario>
        <Stack 
          direction="row" 
          justifyContent="flex-end"
          spacing={2}
          alignItems="center"
          className='mb-4' 
        >
            <Buttoniu 
                variant="outlined"
                // onClick={nuevoRegistro}
                // id='btn-registro'
                disabled
                className='add-gastos'
                ><AddIcon />
            </Buttoniu>
        </Stack>
        <Row>
            <Col xs={2} md={2} lg={2}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Ticket/Factura</Form.Label>
              <Field
                id='Tiket'
                type="text"
                name='Tiket'
              />
            </Form.Group>
            </Col>

            <Col xs={2} md={2} lg={2}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Concepto</Form.Label>
              <Field
                id='concepto'
                type="text"
                name='concepto'
              />
            </Form.Group>
            </Col>

            <Col xs={2} md={2} lg={2}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Cantidad</Form.Label>
              <Field
                id='cantidad'
                type="text"
                name='cantidad'
              />
            </Form.Group>
            </Col>

            <Col xs={2} md={2} lg={2}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label >Precio Unitario</Form.Label>
              <Field
                id='precioUnitario'
                type="text"
                name='precioUnitario'
              />
            </Form.Group>
            </Col>

            <Col xs={2} md={2} lg={2}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Importe</Form.Label>
              <Field
                id='importe'
                type="text"
                name='importe'
              />
            </Form.Group>
            </Col>
            
            <Col xs={2} md={2} lg={2}>
            <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
              <Form.Label>Observaciones</Form.Label>
              <Field
                id='observaciones'
                type="text"
                name='observaciones'
              />
            </Form.Group>
            </Col>
          </Row>
          <LoadingButton
                type='submit'
                loading={loading}
                variant="outlined"
                ><span>Guardar</span>
            </LoadingButton>
         
        </Formulario>
      )}
    </Formik>
    </>
  )
}