import { useMemo } from 'react';

//chart.js 
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    Filler  } from 'chart.js';
  
  import { Line } from 'react-chartjs-2';
  
  ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
    Filler
  );  

  const labels = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];

  const ChartOptions = {
    maintainAspectRatio: false,
    responsive: true,
    plugins:{
        legend: {
        display: false,
        },
        scales: {
          x: [{
            gridLines: {
              display: false
            }
          }],
          y: [{
            gridLines: {
              display: false
            }
          }]
        }
    },
  }

//   scales: {
//     xAxes: [{
//       gridLines: {
//         display: false
//       }
//     }],
//     yAxes: [{
//       gridLines: {
//         display: false
//       }
//     }]
//   }

export const ChartMeses = () => {
    const data = useMemo(function(){
        return{
            datasets: [
                {
                  fill: true,
                  label: 'Digital Goods',
                  backgroundColor: 'rgba(60,141,188,0.9)',
                  borderColor: 'rgba(60,141,188,0.8)',
                  pointRadius: false,
                  pointColor: '#3b8bba',
                  pointStrokeColor: 'rgba(60,141,188,1)',
                  pointHighlightFill: '#fff',
                  pointHighlightStroke: 'rgba(60,141,188,1)',
                  tension: 0.5,
                  data: [28, 48, 40, 19, 86, 27, 90]
                },
                {
                  fill: true,
                  label: 'Electronics',
                  backgroundColor: 'rgba(210, 214, 222, 1)',
                  borderColor: 'rgba(210, 214, 222, 1)',
                  pointRadius: false,
                  pointColor: 'rgba(210, 214, 222, 1)',
                  pointStrokeColor: '#c1c7d1',
                  pointHighlightFill: '#fff',
                  pointHighlightStroke: 'rgba(220,220,220,1)',
                  tension: 0.5,
                  data: [65, 59, 80, 81, 56, 55, 40]
                }
              ],
              labels
        };
    }, []);
  
    return  <Line data={data} options={ChartOptions} />;
}
