import { useState } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';
import { Formik, Form, Field } from 'formik';
import toast, { Toaster } from 'react-hot-toast';

//material UI
import LoadingButton from '@mui/lab/LoadingButton';

//global
import { Baseurl } from '../../store/Baseurl';
//styles
import './style/index.css';

//store
import { login } from '../../store/auth/authSlice';
import { useDispatch } from 'react-redux';


const Login = () => {
    
    const [loading, setLoading] = useState(false);
    const [respuesta, setRespuesta] = useState({});

    const dispatch = useDispatch();
    
    return (
        <>
    <div className='fondo'>
        <Toaster/>
    <div id='box'>
  {/* /.login-logo */}
  <div className="card card-registro">
    <div className="card-body">
      
      <div className='logo-container'>
      <img src="./images/company_logo.jpg" alt="logo" className='logo-login' />
      </div>

      <Formik
        initialValues={{
          email:'',
          password:''
        }}
        onSubmit={(valores, { resetForm }) =>{
            setLoading(true);

          const enviar = async() =>{
            const { data } = await axios({method:'POST',url:`${Baseurl}login`,data:valores});
            setRespuesta(data);

            if(data.respuesta){toast.error(data.respuesta)}
            setLoading(false);
            
            if(data.accessToken){
                const { accessToken } = data;
                const token = accessToken.split('|');
                document.cookie = `token=${token[1]}; max-age=${60 * 60 * 24}; path=/; samesite=strict`;
                resetForm();    
                setLoading(false);
                dispatch(login(token[1]));
            }
          }
          enviar();
        }}
      >
{() =>(
    <Form>
        <div className='mb-3'>
          <div className="input-group">
            <Field 
                type="email" 
                id='email'
                name='email'
                className="form-control" 
                placeholder="Correo" 
            />
            <div className="input-group-append">
              <div className="input-group-text">
                <span className="fas fa-envelope" />
              </div>
            </div>
          </div>
          {respuesta.email && <div className='error'>{respuesta.email}</div>}
        </div>

        <div className='mb-3'>
          <div className="input-group">
            <Field 
                type="password"
                id='password'
                name='password' 
                className="form-control" 
                placeholder="Contraseña" 
                />
            <div className="input-group-append">
              <div className="input-group-text">
                <span className="fas fa-lock" />
              </div>
            </div>
          </div>
          {respuesta.password && <div className='error'>{respuesta.password}</div>}
        </div>
  
          <div className='boton-container'>
            <LoadingButton
                type='submit'
                loading={loading}
                variant="outlined"
                ><span>Iniciar Sesión</span>
            </LoadingButton>
        </div>
          
        </Form>
        )}
      
     
    </Formik>
     
      {/* <p className="mb-1">
        <a href="forgot-password.html">olvide mi contraseña</a>
      </p> */}
      {/* <p className="mt-3">
      <Link to="/registro" className="text-center">Registrate </Link>
      </p> */}
    </div>
    {/* /.login-card-body */}
  </div>

{/* /.login-box */}

</div>
</div>
    </>
    )
};

export default Login;
