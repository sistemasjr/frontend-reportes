import { useState } from 'react';
import { Link, Navigate, useNavigate } from 'react-router-dom';
import axios from 'axios';
import { Formik, Form ,Field} from 'formik';
import { Baseurl } from '../../store/Baseurl';

import './style/index.css';

import LoadingButton from '@mui/lab/LoadingButton';
import toast, { Toaster } from 'react-hot-toast';

const Registro = () => {

    const [loading, setLoading] = useState(false);
    const [respuesta, setRespuesta] = useState({});
    
  return (
    <>
    <Toaster/>
<div className='fondo'>
<div id='box'>
{/* /.login-logo */}
<div className="card card-registro">
<div className="card-body">
  
  <div className='logo-container'>
  <img src="./images/company_logo.jpg" alt="logo" className='logo-login' />
  </div>

    

  <Formik
    initialValues={{
      name: '',
      email:'',
      password:'',
      password2:''
    }}
    onSubmit={(valores, { resetForm }) =>{
        setLoading(true);
      
        const enviar = async() =>{
            const { data } = await axios({method:'POST',url:`${Baseurl}register`,data:valores});
                setRespuesta(data);
                setLoading(false);

        if(data.respuesta){
            toast.success(data.respuesta);
            resetForm();  
        }
    }
      enviar();

    }}
  >

{() =>(
    <Form>
        <div className='mb-3'>
        <div className="input-group">
            <Field 
                type="text" 
                id='name'
                name='name'
                className="form-control" 
                placeholder="Nombre"
            />
            <div className="input-group-append">
            <div className="input-group-text">
                <span className="fa-solid fa-user" />
            </div>
            </div>
        </div>
        {respuesta.name && <div className='error'>{respuesta.name}</div>}
        </div>
        
        <div className='mb-3'>
        <div className="input-group">
            <Field 
                type="email" 
                id='email'
                name='email'
                className="form-control" 
                placeholder="Email"
            />
            <div className="input-group-append">
            <div className="input-group-text">
                <span className="fas fa-envelope" />
            </div>
            </div>
        </div>
        {respuesta.email && <div className='error'>{respuesta.email}</div>}
        </div>

        <div className='mb-3'>
        <div className="input-group">
            <Field 
                type="password"
                id='password'
                name='password' 
                className="form-control" 
                placeholder="Contraseña"
                />
            <div className="input-group-append">
            <div className="input-group-text">
                <span className="fas fa-lock" />
            </div>
            </div>
        </div>
        {respuesta.password && <div className='error'>{respuesta.password}</div>}
        </div>

        <div className='mb-3'>
        <div className="input-group">
            <Field 
                type="password"
                id='password2'
                name='password2' 
                className="form-control" 
                placeholder="Confirmar Contraseña"
                />
            <div className="input-group-append">
            <div className="input-group-text">
                <span className="fas fa-lock" />
            </div>
            </div>
        </div>
        {respuesta.password2 && <div className='error'>{respuesta.password2}</div>}
        </div>

        <div className='boton-container'>
            <LoadingButton
                type='submit'
                loading={loading}
                variant="outlined"
                ><span>Registrarse</span>
            </LoadingButton>
        </div>
      
    </Form>
)}
  
 
</Formik>
 
  <p className="mt-3">
    <Link to="/login" className="text-center">iniciar Sesión </Link>
  </p>
</div>
{/* /.login-card-body */}
</div>

{/* /.login-box */}

</div>
</div>
</>
)
}
export default Registro
