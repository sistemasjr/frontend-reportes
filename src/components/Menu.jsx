import { NavLink } from "react-router-dom";
import './Menu.css';

export const Menu = () => {
  return (
    <aside className="main-sidebar sidebar-dark-primary elevation-4">
  {/* Brand Logo */}
  <NavLink to='/' className="brand-link">
    <img src="./images/company_logo.png" alt="Logo" className="brand-image elevation-3" style={{opacity: '.8'}} />
    <span className="brand-text font-weight-light"></span>
  </NavLink>
  {/* Sidebar */}
  <div className="sidebar">
   
    {/* SidebarSearch Form */}
    <div className="form-inline"></div>
    {/* Sidebar Menu */}
    <nav className="mt-2">
      <ul className="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        {/* Add icons to the links using the .nav-icon class
         with font-awesome or any other icon font library */}
        
        {/* <div className="link-menu">
            <li className="nav-item">
              <NavLink to='/inicio' className={({ isActive }) => (isActive ? ' active' : '')}> */}
              
              {/* <i class="fa-regular fa-pen-to-square"></i> */}
              
              {/* <i className="fa fa-address-book-o iconos"></i>
                <p className="text-white p-link">Registro de embarques</p>
              
              </NavLink>
            </li>
        </div>     */}
          
        
      </ul>
    </nav>
    {/* /.sidebar-menu */}
  </div>
  {/* /.sidebar */}
</aside>

  )
}
