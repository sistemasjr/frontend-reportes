import { NavLink } from "react-router-dom";
import Button from '@mui/material/Button';
import { useDispatch } from "react-redux";
import { logout } from "../store/auth/authSlice";
import { useNavigate } from "react-router-dom";

export const Header = () => {

  const dispatch = useDispatch();
  const navigate = useNavigate();
  
  const cerrar = () =>{
    dispatch(logout());
    navigate('/login');
    
  }
  return (
    <>
    <nav className="main-header navbar navbar-expand navbar-white navbar-light">
  {/* Left navbar links */}
  <ul className="navbar-nav">
    <li className="nav-item">
      <a className="nav-link" data-widget="pushmenu" href="#" role="button"><i className="fas fa-bars" /></a>
    </li>
    <li className="nav-item d-none d-sm-inline-block">
      <NavLink to='/inicio' className={({ isActive }) => (isActive ? 'nav-link active' : 'nav-link')}>
        inicio
      </NavLink>
    </li>
  </ul>
  {/* Right navbar links */}
  <ul className="navbar-nav ml-auto">
   
   
    {/* Notifications Dropdown Menu */}
    
    <li className="nav-item">
      <a className="nav-link" data-widget="fullscreen" href="#" role="button">
        <i className="fas fa-expand-arrows-alt" />
      </a>
    </li>
    <li className="nav-item">
      <a className="nav-link" data-widget="control-sidebar" data-controlsidebar-slide="true" href="#" role="button">
        <i className="fas fa-th-large" />
      </a>
    </li>
  </ul>
  <Button variant="outlined" onClick={cerrar}>
  Cerrar sesión
</Button>
</nav>

    </>
  )
}
