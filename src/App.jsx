import { useEffect } from 'react';
import { Routes,Route, Navigate } from 'react-router-dom';

//paginas
import Login from './auth/Login';
import Registro from './auth/Registro';
import Home from './pages/Home';

//redux
import { useDispatch,useSelector } from 'react-redux';
import { login, logout } from './store/auth/authSlice';


function App() {
  const dispatch = useDispatch();

  const status = useSelector((state) => state.auth.status)
  console.log(status);
   
useEffect(() =>{
  const token = document.cookie.replace('token=','');
  if(token.length < 1){dispatch(logout())};
  if(token.length > 1){dispatch(login())};
},[])

  if(status === 'checking'){
    return 'cargando';
  }


  return (
      <>
      <Routes>
        {
          (status === 'not-authenticated')
          ?<>
            <Route path='/login' element={<Login/>}/>
            {/* <Route path='/registro' element={<Registro/>}/> */}
            <Route path='/*' element={<Navigate to='/login'/>}/>
           </>
         :<>
            <Route path='/' element={<Home/>}/>
            <Route path='/inicio' element={<Home/>}/>
            <Route path='/*' element={<Navigate to='/inicio'/>}/>
          </>
        }
      </Routes>
      </>
  )
}

export default App
