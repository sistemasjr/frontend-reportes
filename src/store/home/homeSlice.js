import { createSlice } from '@reduxjs/toolkit';
import axios from 'axios';
import { Baseurl } from '../Baseurl';

const initialState = {
  datos: [],
  actualizardatos: false,
  id:null,
  datosmodal:{}
}

export const homeSlice = createSlice({
  name: 'home',
  initialState,
  reducers: {
      getdatos: (state) =>{
          
            if(state.actualizardatos === false){
                state.actualizardatos = true;
            }else{
                state.actualizardatos = false;
            }             
      },
      actualdatos: (state, {payload}) => {
        state.datos = payload;
      },
      idmodal:(state, {payload}) =>{
        state.id = payload;
      },
      datosmodal:(state, {payload}) =>{
        state.datosmodal = payload;
      },
      
  },
})

// Action creators are generated for each case reducer function
export const { getdatos, actualdatos, idmodal, datosmodal } = homeSlice.actions

export default homeSlice.reducer

