
export const Loading = ({isloading}) => {
  return (
    <>
    {
        (isloading === true)
        ?<><img 
                src="./images/loading.gif" 
                style={{"height" : "30px", "width" : "30px"}} 
                alt="cargando..." 
            />
         </>
        :<></>
    }
    </>
  )
}
