import { createSlice } from '@reduxjs/toolkit'

const initialState = {
  status: 'checking', // 'checking' 'not-authenticated' 'authenticated'
  token: '',
}

export const authSlice = createSlice({
  name: 'auth',
  initialState,
  reducers: {
    login: (state) => {
          state.status = 'authenticated';
    },
    logout:(state) => {
      document.cookie = `token=${``}; expires=Thu, 01 Jan 1970 00:00:01 GMT; path=/;`;
      state.status = 'not-authenticated';
      state.token = '';
    },
    checkingCredentials: (state) => {
      state.status = 'checking';
    }
  },
})

// Action creators are generated for each case reducer function
export const { login, logout, checkingCredentials } = authSlice.actions

export default authSlice.reducer